var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT c.*, a.street, a.zip_code FROM account c ' +
        'LEFT JOIN account_address ca on ca.account_id = c.account_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE c.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (account_id) VALUES (?)';

    var queryData = [params.account_name];

    connection.query(query, params.account_name, function(err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO account_ADDRESS
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_address (account_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                accountAddressData.push([account_id, params.address_id[i]]);
            }
        }
        else {
            accountAddressData.push([account_id, params.address_id]);
        }

        // NOTE THE EXTRA [] AROUND accountAddressData
        connection.query(query, [accountAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountAddressInsert = function(account_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_address (account_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < params.address_id.length; i++) {
            accountAddressData.push([account_id, params.address_id[i]]);
        }
    }
    else {
        accountAddressData.push([account_id, params.address_id]);
    }
    connection.query(query, [accountAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressInsert = accountAddressInsert;

//declare the function so it can be used locally
var accountAddressDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_address WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountAddressDeleteAll = accountAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET account_name = ? WHERE account_id = ?';
    var queryData = [params.account_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete account_address entries for this account
        accountAddressDeleteAll(params.account_id, function(err, result){

            if(params.address_id !== null) {
                //insert account_address ids
                accountAddressInsert(params.account_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS account_getinfo;

     DELIMITER //
     CREATE PROCEDURE account_getinfo (account_id int)
     BEGIN

     SELECT * FROM account WHERE account_id = _account_id;

     SELECT a.*, s.account_id FROM address a
     LEFT JOIN account_address s on s.address_id = a.address_id AND account_id = _account_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL account_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};